#pragma once

#include <iostream>

const int MEM_DEPTH{4096};

class Memory {
    public:
        void write(uint16_t addr, uint8_t data);
        [[nodiscard]] uint8_t read(uint16_t addr);

        uint8_t &operator[](uint16_t addr) {
            uint16_t tmp_addr = addr & 0x0fff;
            return _mem_data[addr];
        }

    private:
        uint8_t _mem_data[MEM_DEPTH];
};
