#pragma once

#include <iostream>
#include <sstream>
#include <iomanip>

const int STACK_SIZE{12};

class Stack {
    public:
        Stack() : sz(0) {
            for(int i = 0; i < STACK_SIZE; i++) _stack_data[i] = 0;
        };
        void push(uint16_t addr);
        void pop(void);
        uint16_t peek(void);

        bool is_full(void) { return sz > STACK_SIZE; };
        bool is_empty(void) { return sz == 0; };

        std::string toString() {
            std::stringstream out;

            out << "Stack:\n";
            if(sz == 0) {
                out << "<empty>\n";
            } else {
                for(int i = 0; i < sz; i++) {
                    out << "0x" << std::hex << std::setw(4) << std::setfill('0') << (int)_stack_data[i] << "\n";
                }
            }
            return out.str();
        }
    private:
        uint16_t _stack_data[STACK_SIZE];
        int sz;
};
