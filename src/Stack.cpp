
#include "Stack.h"

void Stack::push(uint16_t addr) {
    if(is_full()) return; /* do nothing on full stack */

    _stack_data[sz++] = addr&0x0fff;
}

void Stack::pop(void) {
    if(is_empty())return; /* nothing to pop */

    _stack_data[sz--] = 0;
}

uint16_t Stack::peek(void) {
    if(is_empty()) return 0;

    return _stack_data[sz-1];
}
