#include "Memory.h"


void Memory::write(uint16_t addr, uint8_t data) {
    uint16_t int_addr = 0x0fff & addr; /* make sure we're only using 12 bit */

    _mem_data[int_addr] = data;
}

uint8_t Memory::read(uint16_t addr) {
    uint16_t int_addr = 0x0fff & addr; /* make sure we're only using 12 bit */

    return _mem_data[int_addr];
}
