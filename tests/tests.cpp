#include <iostream>

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

#include "../src/Memory.h"
#include "../src/Registers.h"
#include "../src/Stack.h"

TEST_CASE( "Memory normal Test", "[memory]" ) {
    Memory mem;

    for (int i = 0; i < 4096; i++) {
        mem.write(i, i&0xff);
    }

    for (int i = 0; i < 4096; i++) {
        REQUIRE( mem.read(i) == (i&0xff));
    }

}

TEST_CASE( "Memory subscript Test", "[memory]" ) {
    Memory mem;

    for (int i = 0; i < 4096; i++) {
        mem[i] = (i+3)&0xff;
    }

    for (int i = 0; i < 4096; i++) {
        REQUIRE( mem[i] == ((i+3)&0xff));
    }

}

TEST_CASE("Register Test", "[registers]") {
    Registers regs;

    for (int i = 0; i < 16; i++) {
        regs.write(i, (i+3)&0xff);  //[i] = (i+3)&0xff;
    }

    for (int i = 0; i < 16; i++) {
        REQUIRE(regs.read(i) == (i+3&0xff));
        /*REQUIRE( regs[i] == ((i+3)&0xff));*/
    }

}

TEST_CASE("Register subscript Test", "[registers]") {
    Registers regs;

    for (int i = 0; i < 16; i++) {
        regs[i] = (i+3)&0xff;
    }

    for (int i = 0; i < 16; i++) {
        REQUIRE( regs[i] == ((i+3)&0xff));
    }

}

TEST_CASE("Stack Test", "[stack]") {
    Stack st;
    uint8_t tmp;
    
    st.push(0x1f);
    st.push(0x02);
    st.push(0xf3);

    REQUIRE(st.peek() == 0x03);
    st.pop();
    REQUIRE(st.peek() == 0x02);
    REQUIRE(st.peek() == 0x02);
    st.pop();
    REQUIRE(st.peek() == 0x0f);
    st.pop();
    REQUIRE(st.peek() == 0x00);
}
