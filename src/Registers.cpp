#include "Registers.h"

void Registers::write(uint8_t addr, uint8_t data) {
    uint8_t int_addr = 0x0f & addr; /* make sure we're only using 12 bit */

    _reg_data[int_addr] = data;
}

uint8_t Registers::read(uint8_t addr) {
    uint8_t int_addr = 0x0f & addr; /* make sure we're only using 12 bit */

    return _reg_data[int_addr];
}
