#pragma once

#include <SDL.h>

#include "Memory.h"
#include "Registers.h"
#include "Stack.h"

const int H_RES = 64;
const int V_RES = 32;

const uint32_t PIXEL_MASK = 0x00ffffff;

struct Pixel {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t pad = 0;

    bool operator==(Pixel const &p) const {
        return (b == p.b) && (g == p.g) && (r == p.r);
    }

    explicit operator bool() const {
        return (b == 0) && (g == 0) & (r == 0);
    }
};

class Chip8 {
    public:
        Chip8(const char *rom_path);
        void tick();
        void reset();

        void testPixel(uint16_t x, uint16_t y) {
            _regs[0xf] = (_screenbuffer[y*H_RES + x]&PIXEL_MASK) ? 1 : 0;
            if(_regs[0xf]) _screenbuffer[y*H_RES + x] ^= PIXEL_MASK;
        }

    private:
        void setPixel(uint16_t x, uint16_t y, uint8_t val);
        void displayInit();
        void displayRefresh();
        void displayClear();
        
        void checkEvents();
        uint8_t waitKey();

        Memory _mem;
        Registers _regs;
        Stack _stk;

        uint16_t _pc;
        uint16_t _sp;
        uint16_t _opcode;
        uint16_t _mem_addr;
        
        uint8_t _keypad[16];
        uint32_t _screenbuffer[H_RES*V_RES];

        uint8_t _delay_timer;
        uint8_t _sound_timer;

        bool _redraw;
        SDL_Window *_sdl_window;
        SDL_Renderer *_sdl_renderer;
        SDL_Texture *_sdl_texture;
};
