#pragma once

#include <iostream>
#include <sstream>
#include <cmath>
#include <iomanip>

const int NUM_REGS{16};
const int NUM_REGS_MASK{static_cast<int>(std::ceil(log2(NUM_REGS)))-1};

class Registers {
    public:
        void write(uint8_t addr, uint8_t data);
        [[nodiscard]] uint8_t read(uint8_t addr);

        uint8_t &operator[](uint16_t addr) {
            uint16_t tmp_addr = addr & 0x0f;
            return _reg_data[addr];
        }

        std::string toString() {
            std::stringstream out;

            out << "Registers:\n";
            for(int i = 0; i < NUM_REGS; i++) {
                out << i << ": 0x" << std::hex << std::setw(4) << std::setfill('0') << (int)_reg_data[i] << "\n";
            }
            return out.str();
        }
    private:
        uint8_t _reg_data[NUM_REGS];
};
