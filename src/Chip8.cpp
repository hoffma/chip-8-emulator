/*
 * Current Status 23.09.2022
 * - Kinda working
 * - Display issues (maybe collision not working properly?)
 * - input needs to be checkd
 * - make faster
 * - maybe show register and variables in GUI (maybe use imgui or something)
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdlib.h> /* srand, rand */
#include <time.h> /* time */
#include <sstream>

#include "Chip8.h"

const int FONT_BASE {0x050};

// Keypad keymap
uint8_t keymap[16] = {
    SDLK_x,
    SDLK_1,
    SDLK_2,
    SDLK_3,
    SDLK_q,
    SDLK_w,
    SDLK_e,
    SDLK_a,
    SDLK_s,
    SDLK_d,
    SDLK_z,
    SDLK_c,
    SDLK_4,
    SDLK_r,
    SDLK_f,
    SDLK_v,
};

unsigned char chip8_fontset[80] =
{
    0xF0, 0x90, 0x90, 0x90, 0xF0, //0
    0x20, 0x60, 0x20, 0x20, 0x70, //1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, //2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, //3
    0x90, 0x90, 0xF0, 0x10, 0x10, //4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, //5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, //6
    0xF0, 0x10, 0x20, 0x40, 0x40, //7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, //8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, //9
    0xF0, 0x90, 0xF0, 0x90, 0x90, //A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, //B
    0xF0, 0x80, 0x80, 0x80, 0xF0, //C
    0xE0, 0x90, 0x90, 0x90, 0xE0, //D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, //E
    0xF0, 0x80, 0xF0, 0x80, 0x80  //F
};

Chip8::Chip8(const char *rom_path) : _pc(0x0200), _sp(0x0), _opcode(0x0) {

    /* clear stack */
    while(!_stk.is_empty()) _stk.pop();
    /* clear registers */
    /* clear keys */
    for(int i = 0; i < 16; i++) {
        _regs.write(i, 0);
        _keypad[i] = 0;
    }

    /* load font */
    for(int i = 0x050; i < 0x0a0; i++) {
        _mem.write(i, chip8_fontset[i - 0x050]);
    }

    /* reset timers */
    _delay_timer = 0;
    _sound_timer = 0;

    /* read rom */

    FILE *rom = fopen(rom_path, "rb");
    if(rom == nullptr) {
        std::cerr << "Error reading ROM" << std::endl;
        exit(EXIT_FAILURE);
    }

    fseek(rom, 0, SEEK_END);
    long rom_size = ftell(rom);
    rewind(rom);

    // Allocate memory to store rom
    char* rom_buffer = (char*) malloc(sizeof(char) * rom_size);
    if (rom_buffer == NULL) {
        std::cerr << "Failed to allocate memory for ROM" << std::endl;
        exit(EXIT_FAILURE);
    }

    // Copy ROM into buffer
    size_t result = fread(rom_buffer, sizeof(char), (size_t)rom_size, rom);
    if (result != rom_size) {
        std::cerr << "Failed to read ROM" << std::endl;
        exit(EXIT_FAILURE);
    }

    // Copy buffer to memory
    if ((4096-512) > rom_size){
        for (int i = 0; i < rom_size; ++i) {
            _mem[i + 512] = (uint8_t)rom_buffer[i];   // Load into memory starting
                                                        // at 0x200 (=512)
        }
    }
    else {
        std::cerr << "ROM too large to fit in memory" << std::endl;
        exit(EXIT_FAILURE);
    }

    // Clean up
    fclose(rom);
    free(rom_buffer);

    /* for random numbers */
    srand(time(nullptr));

    this->displayInit();
    this->displayClear();
}

void Chip8::reset() {
    _pc = 0x0200;
    _sp = 0x0;
    _opcode = 0x0;
}

void Chip8::tick() {
    uint8_t tmp;

    /* fetch */
    _opcode = _mem[_pc+1] | (_mem[_pc] << 8);

    /* increment pc */
    _pc += 2;

    /* check events for input or exit */
    this->checkEvents();

    /* decode & execute */
    /*uint16_t tmp, vx, vy;*/
    uint8_t op_num = (_opcode >> 12) & 0xf;

    uint8_t X = (_opcode & 0x0f00) >> 8;
    uint8_t Y = (_opcode & 0x00f0) >> 4;
    uint16_t vx = (uint16_t) _regs[X];
    uint16_t vy = (uint16_t) _regs[Y];
    uint8_t tmpx = 0, tmpy = 0;
    uint16_t NNN = (_opcode & 0x0fff);
    uint16_t NN = (_opcode & 0x00ff);
    uint16_t N = (_opcode & 0x000f);

    uint16_t tmp_erg, tmp_flag;

    switch(op_num) {
        case 0x0:
            if(_opcode == 0x0000) {
                std::cerr << "Unknown opcode 0x0000" << std::endl;
                exit(EXIT_FAILURE);
            } else if(_opcode == 0x00e0) {
                this->displayClear();
            } else if (_opcode == 0x00ee) {
                _pc = _stk.peek();
                _stk.pop();
            } else {
                std::cerr << "unknown opcode 0x" << std::hex << std::setw(4) << std::setfill('0') << _opcode << std::endl;
                exit(EXIT_FAILURE);
                /*_stk.push(_pc);
                _pc = NNN;*/
            }
            break;
        case 0x1: /* goto */
            _pc = NNN;
            break;
        case 0x2: /* call subroutine */
            _stk.push(_pc);
            _pc = NNN;
            break;
        case 0x3: /* skip next instruction if vx == NN */
            if (vx == NN) _pc += 2;
            break;
        case 0x4: /* skip next instruction if vx != NN */
            if (vx != NN) _pc += 2;
            break;
        case 0x5: /* skip next instruction if vx == vy */
            if (vx == vy) _pc += 2;
            break;
        case 0x6: /* set vx to NN */
            _regs[X] = NN;
            break;
        case 0x7: /* add  NN to vx */
            _regs[X] += NN;
            break;
        case 0x8:
            /* TODO: carry things */
            switch(_opcode & 0x000f) {
                case 0x0:
                    _regs[X] = vy;
                    break;
                case 0x1:
                    _regs[X] |= vy;
                    break;
                case 0x2:
                    _regs[X] &= vy;
                    break;
                case 0x3:
                    _regs[X] ^= vy;
                    break;
                case 0x4:
                    tmp_erg = _regs[X] + vy;
                    _regs[0xf] = (tmp_erg >> 8) & 0x1; // save flag
                    _regs[X] += vy;
                    break;
                case 0x5:
                    _regs[0xf] = (vx > vy) ? 1 : 0;
                    _regs[X] -= vy;
                    break;
                case 0x6:
                    _regs[0xf] = vx&0x1; // save LSB
                    _regs[X] >>= vy;
                    break;
                case 0x7:
                    _regs[0xf] = (vy > vx) ? 1 : 0;
                    _regs[X] = vy - vx;
                    break;
                case 0xE:
                    _regs[0xf] = (vx >> 7)&0x1; // save MSB
                    _regs[X] <<= vy;
                    break;
            }
            break;
        case 0x9: /* skip the next instruction if vx != vy */
            if (vx != vy) _pc += 2;
            break;
        case 0xa: /* set I to NNN */
            _mem_addr = NNN;
            break;
        case 0xb: /* jump to NNN + v0 */
            _pc = NNN + _regs[0];
            break;
        case 0xc: /* generate random number */
            tmp = (rand()%256) & NN;
            _regs[X] = tmp; // rand()%256; /* random number 0-255 */
            break;
        case 0xd: /* TODO: sprite generation */
            _regs[0xf] = 0;
            for(int j = 0; j < N; j++) {
                for(int i = 0; i < 8; i++) {
                    tmpx = (vx + i)%H_RES;
                    tmpy = (vy + j)%V_RES;
                    _regs[0xf] |= (_screenbuffer[tmpy*H_RES + tmpx]&PIXEL_MASK) ? 1 : 0;

                    tmp = (_mem[_mem_addr + j] >> (8-i))&0x1;
                    if(tmp) {
                        _screenbuffer[tmpy*H_RES + tmpx] ^= PIXEL_MASK;
                    } else {
                        _screenbuffer[tmpy*H_RES + tmpx] ^= 0x0;
                    }
                }
                _redraw = true;
            }
            break;
        case 0xe: /* keyboard input */
            switch(_opcode&0x00ff) {
                case 0x9E:
                    if(_keypad[vx]) _pc += 2;
                    break;
                case 0xA1:
                    if(!_keypad[vx]) _pc += 2;
                    break;
            }
            break;
        case 0xf: /* misc */
            switch(_opcode&0x00ff) {
                case 0x07:
                    _regs[X] = _delay_timer;
                    break;
                case 0x0A:
                    _regs[X] = waitKey();
                    break;
                case 0x15:
                    _delay_timer = vx;
                    break;
                case 0x18:
                    _sound_timer = vx;
                    break;
                case 0x1E: 
                    _mem_addr += vx;
                    break;
                case 0x29: 
                    _mem_addr = FONT_BASE + vx*5;
                    break;
                case 0x33: break;
                    std::cerr << "TODO: Implement BCD decoding" << std::endl;
                    exit(EXIT_FAILURE);
                case 0x55:
                    for(int i = 0; i < X; i++)
                        _mem[_mem_addr + i] = _regs[i];
                    break;
                case 0x65:
                    for(int i = 0; i < X; i++) {
                        _regs[i] = _mem[_mem_addr + i];
                    }
                    
                   break;
            }
            break;
    }

    /* execute */

    if(_delay_timer > 0) {
        _delay_timer--;
    }

    if(_sound_timer > 0) {
        _sound_timer--;
        // TODO: play sound
    }

    if(_redraw)
        displayRefresh();
}

void Chip8::displayInit() {
    if(SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "SDL init failed" << std::endl;
        exit(EXIT_FAILURE);
    }

    SDL_DisplayMode dm;

    if (SDL_GetDesktopDisplayMode(0, &dm) != 0)
    {
         std::cerr << "SDL_GetDesktopDisplayMode failed: %s" << SDL_GetError() << std::endl;
         exit(EXIT_FAILURE);
    }

    /* chip-8 window size shall be 1/4 of the screen resolution */
    int WINDOW_H_RES, WINDOW_V_RES;
    WINDOW_H_RES = dm.w/2;
    WINDOW_V_RES = dm.h/2;
    std::cerr << "WINDOW_H_RES " << WINDOW_H_RES << ", WINDOW_V_RES " << WINDOW_V_RES << std::endl;

    _sdl_window = nullptr;
    _sdl_renderer = nullptr;
    _sdl_texture = nullptr;

    _sdl_window = SDL_CreateWindow("Chip-8", SDL_WINDOWPOS_CENTERED, 
            SDL_WINDOWPOS_CENTERED, WINDOW_H_RES, WINDOW_V_RES, SDL_WINDOW_SHOWN);
    if(!_sdl_window) {
        std::cerr << "[displayInit] Window creation failed: " << SDL_GetError() << std::endl;
        exit(EXIT_FAILURE);
    }

    _sdl_renderer = SDL_CreateRenderer(_sdl_window, -1,
            SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if(!_sdl_renderer) {
        std::cerr << "[displayInit] Renderer creation failed: " << SDL_GetError() << std::endl;
        exit(EXIT_FAILURE);
    }

    _sdl_texture = SDL_CreateTexture(_sdl_renderer, SDL_PIXELFORMAT_BGR888,
            SDL_TEXTUREACCESS_TARGET, H_RES, V_RES);
    /*_sdl_texture = SDL_CreateTexture(_sdl_renderer, SDL_PIXELFORMAT_INDEX1MSB,
            SDL_TEXTUREACCESS_TARGET, H_RES, V_RES);*/
    if(!_sdl_texture) {
        std::cerr << "[displayInit] Texture creation failed: " << SDL_GetError() << std::endl;
        exit(EXIT_FAILURE);
    }
}

void Chip8::checkEvents() {
    // check for quit event
    SDL_Event e;
    if (SDL_PollEvent(&e)) {
        if (e.type == SDL_QUIT) {
            std::cerr << "Quitting. Bye." << std::endl;
            exit(EXIT_SUCCESS);
        } else if(e.type == SDL_KEYDOWN) {
            switch(e.key.keysym.sym) {
                case SDLK_x: _keypad[0] = 1; break;
                case SDLK_1: _keypad[1] = 1; break;
                case SDLK_2: _keypad[2] = 1; break;
                case SDLK_3: _keypad[3] = 1; break;
                case SDLK_q: _keypad[4] = 1; break;
                case SDLK_w: _keypad[5] = 1; break;
                case SDLK_e: _keypad[6] = 1; break;
                case SDLK_a: _keypad[7] = 1; break;
                case SDLK_s: _keypad[8] = 1; break;
                case SDLK_d: _keypad[9] = 1; break;
                case SDLK_y: _keypad[10] = 1; break;
                case SDLK_c: _keypad[11] = 1; break;
                case SDLK_4: _keypad[12] = 1; break;
                case SDLK_r: _keypad[13] = 1; break;
                case SDLK_f: _keypad[14] = 1; break;
                case SDLK_v: _keypad[15] = 1; break;
                case SDLK_ESCAPE:
                     std::cerr << "Bye" << std::endl;
                     exit(EXIT_SUCCESS);
                     break;
            }
        } else if(e.type == SDL_KEYUP) {
            switch(e.key.keysym.sym) {
                case SDLK_x: _keypad[0] = 0; break;
                case SDLK_1: _keypad[1] = 0; break;
                case SDLK_2: _keypad[2] = 0; break;
                case SDLK_3: _keypad[3] = 0; break;
                case SDLK_q: _keypad[4] = 0; break;
                case SDLK_w: _keypad[5] = 0; break;
                case SDLK_e: _keypad[6] = 0; break;
                case SDLK_a: _keypad[7] = 0; break;
                case SDLK_s: _keypad[8] = 0; break;
                case SDLK_d: _keypad[9] = 0; break;
                case SDLK_y: _keypad[10] = 0; break;
                case SDLK_c: _keypad[11] = 0; break;
                case SDLK_4: _keypad[12] = 0; break;
                case SDLK_r: _keypad[13] = 0; break;
                case SDLK_f: _keypad[14] = 0; break;
                case SDLK_v: _keypad[15] = 0; break;
            }
        }
    }
}

uint8_t Chip8::waitKey() {
    // check for quit event
    SDL_Event e;
    while(1) {
        if (SDL_PollEvent(&e)) {
            if(e.type == SDL_KEYDOWN) {
                switch(e.key.keysym.sym) {
                    case SDLK_x: return 0x0;
                    case SDLK_1: return 0x1;
                    case SDLK_2: return 0x2;
                    case SDLK_3: return 0x3;
                    case SDLK_q: return 0x4;
                    case SDLK_w: return 0x5;
                    case SDLK_e: return 0x6;
                    case SDLK_a: return 0x7;
                    case SDLK_s: return 0x8;
                    case SDLK_d: return 0x9;
                    case SDLK_y: return 0xa;
                    case SDLK_c: return 0xb;
                    case SDLK_4: return 0xc;
                    case SDLK_r: return 0xd;
                    case SDLK_f: return 0xe;
                    case SDLK_v: return 0xf;
                    case SDLK_ESCAPE:
                         std::cerr << "[waitKey] Bye" << std::endl;
                         exit(EXIT_SUCCESS);
                         break;
                }
            }
        }
    }
    return 0;
}

void Chip8::displayRefresh() {
    SDL_UpdateTexture(_sdl_texture, NULL, _screenbuffer, H_RES*sizeof(Pixel));
    SDL_RenderClear(_sdl_renderer);
    SDL_RenderCopy(_sdl_renderer, _sdl_texture, NULL, NULL);
    SDL_RenderPresent(_sdl_renderer);

    _redraw = false;
}

void Chip8::displayClear() {
    for(int i = 0; i < (H_RES*V_RES); i++) {
        _screenbuffer[i] = 0;
    }
    _redraw = true;
}

void Chip8::setPixel(uint16_t x, uint16_t y, uint8_t val) {
    if(x >= 64 || y >= 32) {
        std::cerr << "Pixel coordinate out of range" << std::endl;
        exit(EXIT_FAILURE);
    }

    _screenbuffer[y*H_RES + x] = val ? PIXEL_MASK : 0x0;

    _redraw = true;
}
