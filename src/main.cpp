#include <iostream>
#include <SDL.h>
#include <unistd.h>

#include "Chip8.h"

using namespace std;

int main(int argc, char *argv[]) {
    if(argc < 2) {
        cerr << "Usage: ./chip8 rom_path" << endl;
        exit(EXIT_FAILURE);
    }

    Chip8 chp(argv[1]);

    while(1) {
        chp.tick();
        usleep(1);
    }

    return 0;
}

